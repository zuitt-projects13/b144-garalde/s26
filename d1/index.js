// use the rewquire directive to load node.js modules
// modules is a software component or part of a program that contains one or more routines



// lets node.js to transfer data using hypertext transfer protocol. It is a set of individual files that contain code to create a "component" that helps establish data transfer between applications


// clients (browser) and server (nodeJS/expressJS application) communicate by exchaging individual messages.

// message sent by the client, usuallt a web browser are called request
// http://home
// message sent by the server as an answer are called responses

let http = require("http");

// createServer() method - used to create an HTTP server that LISTENS to request on a specified PORT ang gives response back to the client (.listen(PORT))
// it accepts a function and allows us to perform a certain task for our server

http.createServer(function(request, response){
	// use the writeHead() method to:
	// 1. set a status code for the response 200 means OK/successful
	// 2. set the contect-type of the reponse as a plain tet message
	// writeHead(statuscode,content type)
	response.writeHead(200, {'Content-Type': 'text/plain'});

	// a method to end the response process. can be empty end()
	response.end('Goodbye')



}).listen(4000)


// when a server is running, console will print this message
console.log('Server running at localhost:4000')



// node index.js IN GITBASH
// http://localhost:4000
// enter the URL above in browser

// ctrl + c to stop running
// need to rerun index.if there is an update

// statuscode 200 above OK connection; 400 client problem; 500 server error;

